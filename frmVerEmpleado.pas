unit frmVerEmpleado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm8 = class(TForm)
    LTitulo: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LNombre: TLabel;
    LApellidos: TLabel;
    LCorreo: TLabel;
    LTelf: TLabel;
    LDireccion: TLabel;
    BCerrar: TButton;
    Label6: TLabel;
    LHorario: TLabel;
  private
    { Private declarations }
  public
    constructor Create(aNombre, aApellidos, aCorreo, aDireccion, aHorario: string; aTelefono, aCodigo: Integer); overload;
  end;

var
  Form8: TForm8;
  Codigo: Integer;

implementation

{$R *.dfm}

{ TForm8 }

constructor TForm8.Create(aNombre, aApellidos, aCorreo, aDireccion,
  aHorario: string; aTelefono, aCodigo: Integer);
begin
  inherited Create(Owner);
  LTitulo.Caption := 'Datos de ' + aNombre;
  LNombre.Caption := aNombre;
  LApellidos.Caption := aApellidos;
  LCorreo.Caption := aCorreo;
  LTelf.Caption := IntToStr(aTelefono);
  LDireccion.Caption := aDireccion;
  LHorario.Caption := aHorario;
  Codigo := aCodigo;
end;

end.
