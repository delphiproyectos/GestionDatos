object Form6: TForm6
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Agrega cliente'
  ClientHeight = 242
  ClientWidth = 477
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 29
    Top = 24
    Width = 89
    Height = 23
    Caption = 'Nombre: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 17
    Top = 61
    Width = 101
    Height = 23
    Caption = 'Apellidos: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 41
    Top = 101
    Width = 77
    Height = 23
    Caption = 'Correo: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 22
    Top = 141
    Width = 96
    Height = 23
    Caption = 'Tel'#233'fono: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 17
    Top = 178
    Width = 102
    Height = 23
    Caption = 'Direcci'#243'n: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object TNombre: TEdit
    Left = 124
    Top = 29
    Width = 149
    Height = 21
    MaxLength = 10
    TabOrder = 0
  end
  object TApellidos: TEdit
    Left = 124
    Top = 66
    Width = 149
    Height = 21
    MaxLength = 25
    TabOrder = 1
  end
  object TCorreo: TEdit
    Left = 124
    Top = 106
    Width = 149
    Height = 21
    MaxLength = 45
    TabOrder = 2
  end
  object TTelf: TEdit
    Left = 124
    Top = 146
    Width = 149
    Height = 21
    MaxLength = 9
    NumbersOnly = True
    TabOrder = 3
  end
  object TDireccion: TEdit
    Left = 125
    Top = 183
    Width = 149
    Height = 21
    MaxLength = 50
    TabOrder = 4
  end
  object BtnGuardar: TButton
    Left = 310
    Top = 47
    Width = 139
    Height = 60
    Caption = 'Guardar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 5
  end
  object BtnCancelar: TButton
    Left = 310
    Top = 127
    Width = 139
    Height = 60
    Caption = 'Cancelar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 6
  end
end
