unit frmEditarCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm4 = class(TForm)
    LTitulo: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    TNombre: TEdit;
    TApellidos: TEdit;
    TCorreo: TEdit;
    TTelf: TEdit;
    TDireccion: TEdit;
    BtnGuardar: TButton;
    BtnCancelar: TButton;
  private

  public
    constructor Create(aNombre, aApellidos, aCorreo, aDireccion: string; aTelefono, aCodigo: Integer); overload;
  end;

var
  Form4: TForm4;
  Codigo: Integer;

implementation

{$R *.dfm}

{ TForm4 }

constructor TForm4.Create(aNombre, aApellidos, aCorreo, aDireccion: string;
  aTelefono, aCodigo: Integer);
begin
  inherited Create(Owner);
  LTitulo.Caption := 'Datos de ' + aNombre;
  TNombre.Text := aNombre;
  TApellidos.Text := aApellidos;
  TCorreo.Text := aCorreo;
  TTelf.Text := IntToStr(aTelefono);
  TDireccion.Text := aDireccion;
  Codigo := aCodigo;
end;

end.
