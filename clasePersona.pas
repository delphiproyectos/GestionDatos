unit clasePersona;

interface
  uses
    FireDAC.Comp.Client;

  type
    TPersona = class(TObject)
    private
      FNombre: string;
      FApellidos: string;
      FCorreo: string;
      FTelefono: Integer;
      FDireccion: string;
      FCodigo: Integer;
      procedure SetNombre(const Value: string);
      procedure SetApellidos(const Value: string);
      procedure SetCorreo(const Value: string);
      procedure SetTelefono(const Value: Integer);
      procedure SetDireccion(const Value: string);
    procedure SetCodigo(const Value: Integer);
    public
      procedure Insertar; virtual; abstract;
      procedure Actualizar; virtual; abstract;
      procedure Ver; virtual; abstract;
      procedure Eliminar; virtual; abstract;
      constructor Create(aConexion: TFDConnection); overload;
      constructor Create(aNombre, aApellidos, aCorreo, aDireccion: string; aTelefono, aCodigo: Integer; aConexion: TFDConnection); overload;
    protected
      Conexion: TFDConnection;
    published
      property Nombre: string read FNombre write SetNombre;
      property Apellidos: string read FApellidos write SetApellidos;
      property Correo: string read FCorreo write SetCorreo;
      property Telefono: Integer read FTelefono write SetTelefono;
      property Direccion: string read FDireccion write SetDireccion;
      property Codigo: Integer read FCodigo write SetCodigo;
    end;

implementation

{ TPersona }

constructor TPersona.Create(aNombre, aApellidos, aCorreo, aDireccion: string;
  aTelefono, aCodigo: Integer; aConexion: TFDConnection);
begin
  inherited Create;
  FNombre := aNombre;
  FApellidos := aApellidos;
  FCorreo := aCorreo;
  FTelefono := aTelefono;
  FDireccion := aDireccion;
  FCodigo := aCodigo;
  Conexion := aConexion;
end;

constructor TPersona.Create(aConexion: TFDConnection);
begin
  inherited Create;
  Conexion := aConexion;
end;

procedure TPersona.SetApellidos(const Value: string);
begin
  FApellidos := Value;
end;

procedure TPersona.SetCodigo(const Value: Integer);
begin
  FCodigo := Value;
end;

procedure TPersona.SetCorreo(const Value: string);
begin
  FCorreo := Value;
end;

procedure TPersona.SetDireccion(const Value: string);
begin
  FDireccion := Value;
end;

procedure TPersona.SetNombre(const Value: string);
begin
  FNombre := Value;
end;

procedure TPersona.SetTelefono(const Value: Integer);
begin
  FTelefono := Value;
end;

end.
