unit frmPedidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, Data.DB, Vcl.Grids, Vcl.DBGrids, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls;

type
  TForm12 = class(TForm)
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDConnection1: TFDConnection;
    FDQuery2: TFDQuery;
    DataSource1: TDataSource;
    FDQuery1: TFDQuery;
    DBGrid1: TDBGrid;
    FDQuery3: TFDQuery;
    DataSource2: TDataSource;
    IDescripcion: TMemo;
    btnAgregar: TImage;
    btnEliminar: TImage;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
  private
    procedure conectaBBDD;
    procedure actualizaDatos;
    procedure ObtieneDescripcion;
    procedure AsignaDatos;
  public
    { Public declarations }
  end;

var
  Form12: TForm12;

implementation

  uses
    frmAgregaPedido;

  type
    TDatos = record
      Codigo: Integer;
      Precio: Double;
      Cliente: string;
      Empleado: string;
    end;

  var
    Datos: TDatos;
    Filas: Integer;
{$R *.dfm}

{ TForm12 }

procedure TForm12.actualizaDatos;
begin
  FDQuery1.Close();
  FDQuery1.Open();
  FDQuery2.Close();
  FDQuery2.Open();
  Filas := FDQuery2.Fields[0].AsInteger;
end;

procedure TForm12.AsignaDatos;
begin
  if Filas > 0 then
    begin
      Datos.Codigo := StrToInt(dbGrid1.Fields[0].AsString);
      Datos.Precio := StrToFloat(dbGrid1.Fields[1].AsString);
      Datos.Cliente := dbGrid1.Fields[2].AsString;
      Datos.Empleado := dbGrid1.Fields[3].AsString;
    end;
end;

procedure TForm12.btnAgregarClick(Sender: TObject);
var
  form: TForm13;
begin
  form := TForm13.Create(FDConnection1);
  try
    form.ShowModal;
  finally
    form.Free;
  end;
  actualizaDatos;
end;

procedure TForm12.btnEliminarClick(Sender: TObject);
var
  Consulta: TFDQuery;
begin
  Consulta := TFDQuery.Create(nil);
  try
    Consulta.Connection := FDConnection1;
    Consulta.SQL.Text := 'DELETE FROM pedidos WHERE rowid= :Codigo ;';
    consulta.ParamByName('Codigo').AsString := Datos.Codigo.ToString;
    Consulta.ExecSQL;
  finally
    Consulta.Free;
  end;
  actualizaDatos;
end;

procedure TForm12.conectaBBDD;
begin
  FDConnection1.DriverName := 'SQLITE';
  FDConnection1.Params.Values['Database'] := 'Datos.db';
  FDConnection1.Connected := True;
  try
    FDConnection1.Open;
  except on E: EDatabaseError do
    ShowMessage('Ha ocurrido un error');
  end;
end;

procedure TForm12.DBGrid1CellClick(Column: TColumn);
begin
  AsignaDatos;
  ObtieneDescripcion;
end;

procedure TForm12.FormCreate(Sender: TObject);
begin
  conectaBBDD;
  actualizaDatos;
  AsignaDatos;
  ObtieneDescripcion;
end;

procedure TForm12.ObtieneDescripcion;
begin
  FDQuery3.Close;
  FDQuery3.ParamByName('ID').AsString := IntToStr(Datos.Codigo);
  FDQuery3.Open();
  IDescripcion.Text := DataSource2.DataSet.Fields[0].AsString;
end;

end.
