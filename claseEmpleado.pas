unit claseEmpleado;

interface
  uses
    clasePersona,
    FireDAC.Comp.Client;

  type
    TEmpleado = class(TPersona)
    private
      FHorario: string;
      procedure consultaEditar;
      procedure consultaEliminar;
      procedure consultaAgregar;
      procedure SetHorario(const Value: string);
    public
      procedure Insertar; override;
      procedure Actualizar; override;
      procedure Ver; override;
      procedure Eliminar; override;
      constructor Create(aNombre, aApellidos, aCorreo, aDireccion, aHorario: string; aTelefono, aCodigo: Integer; aConexion: TFDConnection); overload;
    protected
    published
      property Horario: string read FHorario write SetHorario;
    end;

implementation

  uses
    frmVerEmpleado,
    frmEditarEmpleado,
    frmEliminarEmpleado,
    frmAgregaEmpleado,
    System.SysUtils;

{ TEmpleado }

procedure TEmpleado.Actualizar;
var
  form: TForm9;
begin
  form := TForm9.Create(Nombre, Apellidos, Correo, Direccion, FHorario, Telefono, Codigo);
  try
    form.ShowModal;
    if form.ModalResult = 1 then
    begin
      Nombre := form.TNombre.Text;
      Apellidos := form.TApellidos.Text;
      Correo := form.TCorreo.Text;
      Direccion := form.TDireccion.Text;
      FHorario := form.IHorario.Text;
      Telefono := StrToInt(form.TTelf.Text);
      consultaEditar;
    end;
  finally
    form.Free;
  end;

end;

procedure TEmpleado.consultaAgregar;
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := Conexion;
    consulta.SQL.Text := 'INSERT INTO empleados(nombre, apellidos, correo, telefono, direccion, horario) VALUES ( :Nombre, :Apellidos, :Correo, :Telefono, :Direccion, :Horario );';
    consulta.ParamByName('Nombre').AsString := Nombre;
    consulta.ParamByName('Apellidos').AsString := Apellidos;
    consulta.ParamByName('Correo').AsString := Correo;
    consulta.ParamByName('Telefono').AsString := IntToStr(Telefono);
    consulta.ParamByName('Direccion').AsString := Direccion;
    consulta.ParamByName('Horario').AsString := FHorario;
    consulta.ExecSQL;
  finally
    consulta.Free;
  end;
end;

procedure TEmpleado.consultaEditar;
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := Conexion;
    consulta.SQL.Text := 'UPDATE empleados SET nombre= :Nombre, apellidos= :Apellidos, correo= :Correo, telefono= :Telefono, direccion= :Direccion, horario= :Horario WHERE rowid= :Codigo;';
    consulta.ParamByName('Nombre').AsString := Nombre;
    consulta.ParamByName('Apellidos').AsString := Apellidos;
    consulta.ParamByName('Correo').AsString := Correo;
    consulta.ParamByName('Telefono').AsString := IntToStr(Telefono);
    consulta.ParamByName('Direccion').AsString := Direccion;
    consulta.ParamByName('Codigo').AsString := IntToStr(Codigo);
    consulta.ParamByName('Horario').AsString := FHorario;
    consulta.ExecSQL;
  finally
    consulta.Free;
  end;
end;

procedure TEmpleado.consultaEliminar;
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := Conexion;
    consulta.SQL.Text := 'DELETE FROM empleados WHERE rowid= :Codigo;';
    consulta.ParamByName('Codigo').AsString := IntToStr(Codigo);
    consulta.ExecSQL;
  finally
    consulta.Free;
  end;
end;

constructor TEmpleado.Create(aNombre, aApellidos, aCorreo, aDireccion,
  aHorario: string; aTelefono, aCodigo: Integer; aConexion: TFDConnection);
begin
  inherited Create(aNombre, aApellidos, aCorreo, aDireccion, aTelefono, aCodigo, aConexion);
  FHorario := aHorario;
end;

procedure TEmpleado.Eliminar;
var
  form: TForm10;
begin
  form := TForm10.Create(Nombre);
  try
    form.ShowModal;
    if form.ModalResult = 1 then
      consultaEliminar;
  finally
    form.Free;
  end;
end;

procedure TEmpleado.Insertar;
var
  form: Tform11;
begin
  form := TForm11.Create(nil);
  try
    form.ShowModal;
    if form.ModalResult = 1 then
    begin
      Nombre := form.TNombre.Text;
      Apellidos := form.TApellidos.Text;
      Correo := form.TCorreo.Text;
      Direccion := form.TDireccion.Text;
      Telefono := StrToInt(form.TTelf.Text);
      FHorario := form.IHorario.Text;
      consultaAgregar;
    end;
  finally
    form.Free;
  end;
end;

procedure TEmpleado.SetHorario(const Value: string);
begin
  FHorario := Value;
end;

procedure TEmpleado.Ver;
var
  form: TForm8;
begin
  form := TForm8.Create(Nombre, Apellidos, Correo, Direccion, FHorario, Telefono, Codigo);
  try
    form.ShowModal;
  finally
    form.Free;
  end;

end;

end.
