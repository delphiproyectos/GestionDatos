object Form3: TForm3
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Datos del cliente'
  ClientHeight = 391
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LTitulo: TLabel
    Left = 84
    Top = 32
    Width = 301
    Height = 34
    Alignment = taCenter
    Caption = 'Datos de {USUARIO}'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -28
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 29
    Top = 112
    Width = 89
    Height = 23
    Caption = 'Nombre: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 17
    Top = 149
    Width = 101
    Height = 23
    Caption = 'Apellidos: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 41
    Top = 189
    Width = 77
    Height = 23
    Caption = 'Correo: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 22
    Top = 229
    Width = 96
    Height = 23
    Caption = 'Tel'#233'fono: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 17
    Top = 266
    Width = 102
    Height = 23
    Caption = 'Direcci'#243'n: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LNombre: TLabel
    Left = 124
    Top = 115
    Width = 79
    Height = 19
    Caption = '{NOMBRE}'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LApellidos: TLabel
    Left = 124
    Top = 152
    Width = 99
    Height = 19
    Caption = '{APELLIDOS}'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LCorreo: TLabel
    Left = 124
    Top = 192
    Width = 79
    Height = 19
    Caption = '{CORREO}'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LTelf: TLabel
    Left = 124
    Top = 232
    Width = 95
    Height = 19
    Caption = '{TELEFONO}'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object LDireccion: TLabel
    Left = 124
    Top = 269
    Width = 101
    Height = 19
    Caption = '{DIRECCION}'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object BCerrar: TButton
    Left = 53
    Top = 312
    Width = 116
    Height = 49
    Caption = 'Cerrar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
  end
  object BPedidos: TButton
    Left = 261
    Top = 312
    Width = 116
    Height = 49
    Caption = 'Ver Pedidos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
end
