unit frmEditarEmpleado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm9 = class(TForm)
    LTitulo: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    TNombre: TEdit;
    TApellidos: TEdit;
    TCorreo: TEdit;
    TTelf: TEdit;
    TDireccion: TEdit;
    BtnGuardar: TButton;
    BtnCancelar: TButton;
    Label6: TLabel;
    IHorario: TComboBox;
  private
    { Private declarations }
  public
    constructor Create(aNombre, aApellidos, aCorreo, aDireccion, aHorario: string; aTelefono, aCodigo: Integer); overload;
  end;

var
  Form9: TForm9;
  Codigo: Integer;

implementation

const
  Horarios: array[0..2] of string = ('Ma�ana', 'Tarde', 'Noche');

{$R *.dfm}

{ TForm9 }

constructor TForm9.Create(aNombre, aApellidos, aCorreo, aDireccion,
  aHorario: string; aTelefono, aCodigo: Integer);
var
  I: Integer;
begin
  inherited Create(Owner);
  LTitulo.Caption := 'Datos de ' + aNombre;
  TNombre.Text := aNombre;
  TApellidos.Text := aApellidos;
  TCorreo.Text := aCorreo;
  TTelf.Text := IntToStr(aTelefono);
  TDireccion.Text := aDireccion;
  IHorario.ItemIndex := 0;
  for I := Low(Horarios) to High(Horarios) - 1 do
  begin
    if Horarios[I] = aHorario then
      IHorario.ItemIndex := I;
  end;
  Codigo := aCodigo;
end;

end.
