unit frmAgregaPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Comp.Client;

type
  TForm13 = class(TForm)
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    BtnGuardar: TButton;
    BtnCancelar: TButton;
    ICliente: TComboBox;
    IEmpleado: TComboBox;
    IPrecio: TEdit;
    IDesc: TMemo;
    procedure BtnGuardarClick(Sender: TObject);
  private
    procedure LlenaClientes;
    procedure LlenaEmpleados;
    procedure InsertaDatos;
  public
    constructor Create(aConexion: TFDConnection); overload;
  end;

var
  Form13: TForm13;

implementation

uses
  System.Generics.Collections;

var
  Conexion: TFDConnection;
  CodigosClientes: TList<Integer>;
  CodigosEmpleados: TList<Integer>;

{$R *.dfm}

{ TForm13 }

procedure TForm13.BtnGuardarClick(Sender: TObject);
begin
  if (ICliente.ItemIndex = -1) or (IEmpleado.ItemIndex = -1) then
  begin
    ShowMessage('Debe introducir un cliente y empleado');
    Self.ModalResult := mrNone;
  end else
  begin
    InsertaDatos;
    Self.ModalResult := mrOk;
  end;
end;

constructor TForm13.Create(aConexion: TFDConnection);
begin
  inherited Create(Owner);
  Conexion := aConexion;
  LlenaClientes;
  LlenaEmpleados;
end;


procedure TForm13.InsertaDatos;
var
  Consulta: TFDQuery;
begin
  Consulta := TFDQuery.Create(nil);
  try
    Consulta.Connection := Conexion;
    Consulta.SQL.Text := 'INSERT INTO pedidos(descripcion, precio, cliente, empleado) VALUES ( :Descripcion, :Precio, :Cliente, :Empleado );';
    consulta.ParamByName('Descripcion').AsString := IDesc.Text;
    consulta.ParamByName('Precio').AsString := IPrecio.Text;
    consulta.ParamByName('Cliente').AsString := IntToStr(CodigosClientes[ICliente.ItemIndex]);
    consulta.ParamByName('Empleado').AsString := IntToStr(CodigosEmpleados[IEmpleado.ItemIndex]);
    Consulta.ExecSQL;
  finally
    Consulta.Free;
  end;
end;

procedure TForm13.LlenaClientes;
var
  Consulta: TFDQuery;
begin
  Consulta := TFDQuery.Create(nil);
  CodigosClientes := TList<Integer>.Create;
  try
    Consulta.Connection := Conexion;
    Consulta.SQL.Text := 'SELECT rowid, nombre, apellidos FROM clientes;';
    Consulta.Open();
    while not Consulta.Eof do
    begin
      ICliente.Items.Add(Consulta.FieldByName('nombre').AsString + ' ' + Consulta.FieldByName('apellidos').AsString);
      CodigosClientes.Add(Consulta.FieldByName('rowid').AsInteger);
      Consulta.Next;
    end;
  finally
    Consulta.Close;
    Consulta.DisposeOf;
  end;
end;

procedure TForm13.LlenaEmpleados;
var
  Consulta: TFDQuery;
begin
  Consulta := TFDQuery.Create(nil);
  CodigosEmpleados := TList<Integer>.Create;
  try
    Consulta.Connection := Conexion;
    Consulta.SQL.Text := 'SELECT rowid, nombre, apellidos FROM empleados;';
    Consulta.Open();
    while not Consulta.Eof do
    begin
      IEmpleado.Items.Add(Consulta.FieldByName('nombre').AsString + ' ' + Consulta.FieldByName('apellidos').AsString);
      CodigosEmpleados.Add(Consulta.FieldByName('rowid').AsInteger);
      Consulta.Next;
    end;
  finally
    Consulta.Close;
    Consulta.DisposeOf;
  end;
end;

end.
