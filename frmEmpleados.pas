unit frmEmpleados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids,
  Vcl.DBGrids, Vcl.Imaging.pngimage, Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TForm7 = class(TForm)
    btnVer: TImage;
    btnEditar: TImage;
    btnAgregar: TImage;
    btnEliminar: TImage;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    FDQuery2: TFDQuery;
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure btnVerClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
  private
    procedure conectaBBDD;
    procedure actualizaDatos;
    procedure AsignaDatos;
  public

  end;

var
  Form7: TForm7;

implementation

  uses
    claseEmpleado;

  type
    TDatos = record
      Codigo: Integer;
      Nombre: string;
      Apellidos: string;
      Correo: string;
      Direccion: string;
      Telefono: Integer;
      Horario: string;
    end;

  var
    Datos: TDatos;
    Filas: Integer;

{$R *.dfm}

{ TForm7 }

procedure TForm7.AsignaDatos;
begin
  if (Filas > 0) then
    begin
      Datos.Codigo := StrToInt(dbGrid1.Fields[0].AsString);
      Datos.Telefono := StrToInt(dbGrid1.Fields[4].AsString);
      Datos.Nombre := dbGrid1.Fields[1].AsString;
      Datos.Apellidos := dbGrid1.Fields[2].AsString;
      Datos.Correo := dbGrid1.Fields[3].AsString;
      Datos.Direccion := dbGrid1.Fields[5].AsString;
      Datos.Horario := dbGrid1.Fields[6].AsString;
    end;
end;

procedure TForm7.btnAgregarClick(Sender: TObject);
var
  Empleado: TEmpleado;
begin
  Empleado := TEmpleado.Create(FDConnection1);
  try
    Empleado.Insertar;
  finally
    Empleado.Free;
  end;
  actualizaDatos;
end;

procedure TForm7.btnEditarClick(Sender: TObject);
var
  Empleado: TEmpleado;
begin
  with Datos do
    Empleado := TEmpleado.Create(Nombre, Apellidos, Correo, Direccion, Horario, Telefono, Codigo, FDConnection1);
    try
      Empleado.Actualizar;
    finally
      Empleado.Free;
    end;
  actualizaDatos;
end;

procedure TForm7.btnEliminarClick(Sender: TObject);
var
  Empleado: TEmpleado;
begin
  with Datos do
    Empleado := TEmpleado.Create(Nombre, Apellidos, Correo, Direccion, Horario, Telefono, Codigo, FDConnection1);
    try
      Empleado.Eliminar;
    finally
      Empleado.Free;
    end;
  actualizaDatos;
end;

procedure TForm7.btnVerClick(Sender: TObject);
var
  Empleado: TEmpleado;
begin
  with Datos do
    Empleado := TEmpleado.Create(Nombre, Apellidos, Correo, Direccion, Horario, Telefono, Codigo, FDConnection1);
    try
      Empleado.Ver;
    finally
      Empleado.Free;
    end;
end;

procedure TForm7.conectaBBDD;
begin
  FDConnection1.DriverName := 'SQLITE';
  FDConnection1.Params.Values['Database'] := 'Datos.db';
  FDConnection1.Connected := True;
  try
    FDConnection1.Open;
  except on E: EDatabaseError do
    ShowMessage('Ha ocurrido un error');
  end;
end;

procedure TForm7.DBGrid1CellClick(Column: TColumn);
begin
  AsignaDatos;
end;

procedure TForm7.FormCreate(Sender: TObject);
begin
  conectaBBDD;
  actualizaDatos;
  AsignaDatos;
end;

procedure TForm7.actualizaDatos;
begin
  FDQuery1.Close();
  FDQuery1.Open();
  FDQuery2.Close();
  FDQuery2.Open();
  Filas := FDQuery2.Fields[0].AsInteger;
end;

end.
