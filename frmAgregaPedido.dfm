object Form13: TForm13
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Agregar pedido'
  ClientHeight = 256
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 52
    Top = 24
    Width = 80
    Height = 23
    Caption = 'Cliente: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 25
    Top = 63
    Width = 107
    Height = 23
    Caption = 'Empleado: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 60
    Top = 101
    Width = 72
    Height = 23
    Caption = 'Precio: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 143
    Width = 124
    Height = 23
    Caption = 'Descripci'#243'n: '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BtnGuardar: TButton
    Left = 326
    Top = 47
    Width = 139
    Height = 60
    Caption = 'Guardar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
    OnClick = BtnGuardarClick
  end
  object BtnCancelar: TButton
    Left = 326
    Top = 127
    Width = 139
    Height = 60
    Caption = 'Cancelar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 1
  end
  object ICliente: TComboBox
    Left = 138
    Top = 29
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 2
  end
  object IEmpleado: TComboBox
    Left = 138
    Top = 74
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 3
  end
  object IPrecio: TEdit
    Left = 138
    Top = 106
    Width = 145
    Height = 21
    MaxLength = 6
    NumbersOnly = True
    TabOrder = 4
    Text = '0'
  end
  object IDesc: TMemo
    Left = 138
    Top = 149
    Width = 145
    Height = 89
    MaxLength = 50
    TabOrder = 5
  end
end
