unit frmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    BtnClientes: TButton;
    BtnEmpleados: TButton;
    BtnPedidos: TButton;
    Label1: TLabel;
    procedure BtnClientesClick(Sender: TObject);
    procedure BtnEmpleadosClick(Sender: TObject);
    procedure BtnPedidosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  frmClientes,
  frmEmpleados,
  frmPedidos;

{$R *.dfm}

procedure TForm1.BtnClientesClick(Sender: TObject);
var
  form: TForm2;
begin
  form := TForm2.Create(nil);
  try
    form.ShowModal;
  finally
    form.Free;
  end;
end;

procedure TForm1.BtnEmpleadosClick(Sender: TObject);
var
  form: TForm7;
begin
  form := TForm7.Create(nil);
  try
    form.ShowModal;
  finally
    form.Free;
  end;
end;

procedure TForm1.BtnPedidosClick(Sender: TObject);
var
  form: TForm12;
begin
  form := TForm12.Create(nil);
  try
    form.ShowModal;
  finally
    form.Free;
  end;
end;

end.
