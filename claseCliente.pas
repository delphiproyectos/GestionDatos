unit claseCliente;

interface
  uses
    clasePersona;

  type
    TCliente = class(TPersona)
    private
      procedure consultaEditar;
      procedure consultaEliminar;
      procedure consultaAgregar;
    public
      procedure Insertar; override;
      procedure Actualizar; override;
      procedure Ver; override;
      procedure Eliminar; override;
    protected
    end;

implementation

uses
  frmVerCliente,
  frmEditarCliente,
  frmEliminarCliente,
  frmAgregaCliente,
  FireDAC.Comp.Client,
  System.SysUtils;

{ TCliente }

procedure TCliente.Actualizar;
var
  form: TForm4;
begin
  form := TForm4.Create(Nombre, Apellidos, Correo, Direccion, Telefono, Codigo);
  try
    form.ShowModal;
    if form.ModalResult = 1 then
    begin
      Nombre := form.TNombre.Text;
      Apellidos := form.TApellidos.Text;
      Correo := form.TCorreo.Text;
      Direccion := form.TDireccion.Text;
      Telefono := StrToInt(form.TTelf.Text);
      consultaEditar;
    end;
  finally
    form.Free;
  end;
end;

procedure TCliente.consultaAgregar;
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := Conexion;
    consulta.SQL.Text := 'INSERT INTO clientes(nombre, apellidos, correo, telefono, direccion) VALUES ( :Nombre, :Apellidos, :Correo, :Telefono, :Direccion);';
    consulta.ParamByName('Nombre').AsString := Nombre;
    consulta.ParamByName('Apellidos').AsString := Apellidos;
    consulta.ParamByName('Correo').AsString := Correo;
    consulta.ParamByName('Telefono').AsString := IntToStr(Telefono);
    consulta.ParamByName('Direccion').AsString := Direccion;
    consulta.ExecSQL;
  finally
    consulta.Free;
  end;
end;

procedure TCliente.consultaEditar;
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := Conexion;
    consulta.SQL.Text := 'UPDATE clientes SET nombre= :Nombre, apellidos= :Apellidos, correo= :Correo, telefono= :Telefono, direccion= :Direccion WHERE rowid= :Codigo;';
    consulta.ParamByName('Nombre').AsString := Nombre;
    consulta.ParamByName('Apellidos').AsString := Apellidos;
    consulta.ParamByName('Correo').AsString := Correo;
    consulta.ParamByName('Telefono').AsString := IntToStr(Telefono);
    consulta.ParamByName('Direccion').AsString := Direccion;
    consulta.ParamByName('Codigo').AsString := IntToStr(Codigo);
    consulta.ExecSQL;
  finally
    consulta.Free;
  end;
end;

procedure TCliente.consultaEliminar;
var
  consulta: TFDQuery;
begin
  consulta := TFDQuery.Create(nil);
  try
    consulta.Connection := Conexion;
    consulta.SQL.Text := 'DELETE FROM clientes WHERE rowid= :Codigo;';
    consulta.ParamByName('Codigo').AsString := IntToStr(Codigo);
    consulta.ExecSQL;
  finally
    consulta.Free;
  end;
end;

procedure TCliente.Eliminar;
var
  form: TForm5;
begin
  form := TForm5.Create(Nombre);
  try
    form.ShowModal;
    if form.ModalResult = 1 then
      consultaEliminar;
  finally
    form.Free;
  end;
end;

procedure TCliente.Insertar;
var
  form: Tform6;
begin
  form := TForm6.Create(nil);
  try
    form.ShowModal;
    if form.ModalResult = 1 then
    begin
      Nombre := form.TNombre.Text;
      Apellidos := form.TApellidos.Text;
      Correo := form.TCorreo.Text;
      Direccion := form.TDireccion.Text;
      Telefono := StrToInt(form.TTelf.Text);
      consultaAgregar;
    end;
  finally
    form.Free;
  end;

end;

procedure TCliente.Ver;
var
  form: TForm3;
begin
  form := TForm3.Create(Nombre, Apellidos, Correo, Direccion, Telefono, Codigo);
  try
    form.ShowModal;
  finally
    form.Free;
  end;

end;

end.
