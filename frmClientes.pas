unit frmClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteDef, FireDAC.Phys.SQLite, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids,
  Data.Bind.EngExt, Vcl.Bind.DBEngExt, Vcl.Bind.Grid, System.Rtti,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components,
  Data.Bind.Grid, Data.Bind.DBScope, Vcl.Imaging.pngimage, Vcl.ExtCtrls;

type
  TForm2 = class(TForm)
    FDConnection1: TFDConnection;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDQuery1: TFDQuery;
    btnVer: TImage;
    btnEditar: TImage;
    btnAgregar: TImage;
    btnEliminar: TImage;
    FDQuery2: TFDQuery;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    procedure btnVerClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
  private
    procedure conectaBBDD;
    procedure actualizaDatos;
    procedure AsignaDatos;
  public

  end;

var
  Form2: TForm2;

implementation

  uses
    claseCliente;

  type
    TDatos = record
      Codigo: Integer;
      Nombre: string;
      Apellidos: string;
      Correo: string;
      Direccion: string;
      Telefono: Integer;
    end;

  var
    Datos: TDatos;
    Filas: Integer;

{$R *.dfm}

{ TForm2 }

procedure TForm2.actualizaDatos;
begin
  FDQuery1.Close();
  FDQuery1.Open();
  FDQuery2.Close();
  FDQuery2.Open();
  Filas := FDQuery2.Fields[0].AsInteger;
end;

procedure TForm2.AsignaDatos;
begin
  if Filas > 0 then
    begin
      Datos.Codigo := StrToInt(dbGrid1.Fields[0].AsString);
      Datos.Telefono := StrToInt(dbGrid1.Fields[4].AsString);
      Datos.Nombre := dbGrid1.Fields[1].AsString;
      Datos.Apellidos := dbGrid1.Fields[2].AsString;
      Datos.Correo := dbGrid1.Fields[3].AsString;
      Datos.Direccion := dbGrid1.Fields[5].AsString;
    end;
end;

procedure TForm2.btnAgregarClick(Sender: TObject);
var
  Cliente: TCliente;
begin
  Cliente := TCliente.Create(FDConnection1);
  try
    Cliente.Insertar;
  finally
    Cliente.Free;
  end;
  actualizaDatos;
end;

procedure TForm2.btnEditarClick(Sender: TObject);
var
  Cliente: TCliente;
begin
  with Datos do
    Cliente := TCliente.Create(Nombre, Apellidos, Correo, Direccion, Telefono, Codigo, FDConnection1);
    try
      Cliente.Actualizar;
    finally
      Cliente.Free;
    end;
  actualizaDatos;
end;

procedure TForm2.btnEliminarClick(Sender: TObject);
var
  Cliente: TCliente;
begin
  with Datos do
    Cliente := TCliente.Create(Nombre, Apellidos, Correo, Direccion, Telefono, Codigo, FDConnection1);
    try
      Cliente.Eliminar;
    finally
      Cliente.Free;
    end;
  actualizaDatos;
end;

procedure TForm2.btnVerClick(Sender: TObject);
var
  Cliente: TCliente;
begin
  with Datos do
    Cliente := TCliente.Create(Nombre, Apellidos, Correo, Direccion, Telefono, Codigo, FDConnection1);
    try
      Cliente.Ver;
    finally
      Cliente.Free;
    end;
end;

procedure TForm2.conectaBBDD;
begin
  FDConnection1.DriverName := 'SQLITE';
  FDConnection1.Params.Values['Database'] := 'Datos.db';
  FDConnection1.Connected := True;
  try
    FDConnection1.Open;
  except on E: EDatabaseError do
    ShowMessage('Ha ocurrido un error');
  end;
end;

procedure TForm2.DBGrid1CellClick(Column: TColumn);
begin
  AsignaDatos;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  conectaBBDD;
  actualizaDatos;
  AsignaDatos;
end;

end.
