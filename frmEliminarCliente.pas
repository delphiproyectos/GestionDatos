unit frmEliminarCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm5 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    LNombre: TLabel;
    BtnEliminar: TButton;
    BtnCancelar: TButton;
  private
    { Private declarations }
  public
    constructor Create(aNombre: string); overload;
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

{ TForm5 }

constructor TForm5.Create(aNombre: string);
begin
  inherited Create(Owner);
  LNombre.Caption := aNombre;
end;

end.
