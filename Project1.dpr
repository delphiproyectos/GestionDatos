program Project1;

uses
  Vcl.Forms,
  frmPrincipal in 'frmPrincipal.pas' {Form1},
  frmClientes in 'frmClientes.pas' {Form2},
  clasePersona in 'clasePersona.pas',
  claseCliente in 'claseCliente.pas',
  frmVerCliente in 'frmVerCliente.pas' {Form3},
  frmEditarCliente in 'frmEditarCliente.pas' {Form4},
  frmEliminarCliente in 'frmEliminarCliente.pas' {Form5},
  frmAgregaCliente in 'frmAgregaCliente.pas' {Form6},
  frmEmpleados in 'frmEmpleados.pas' {Form7},
  claseEmpleado in 'claseEmpleado.pas',
  frmVerEmpleado in 'frmVerEmpleado.pas' {Form8},
  frmEditarEmpleado in 'frmEditarEmpleado.pas' {Form9},
  frmEliminarEmpleado in 'frmEliminarEmpleado.pas' {Form10},
  frmAgregaEmpleado in 'frmAgregaEmpleado.pas' {Form11},
  frmPedidos in 'frmPedidos.pas' {Form12},
  frmAgregaPedido in 'frmAgregaPedido.pas' {Form13};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
