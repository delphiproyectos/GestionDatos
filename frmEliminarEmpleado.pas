unit frmEliminarEmpleado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm10 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    LNombre: TLabel;
    BtnEliminar: TButton;
    BtnCancelar: TButton;
  private
    { Private declarations }
  public
    constructor Create(aNombre: string); overload;
  end;

var
  Form10: TForm10;

implementation

{$R *.dfm}

{ TForm10 }

constructor TForm10.Create(aNombre: string);
begin
  inherited Create(Owner);
  LNombre.Caption := aNombre;
end;

end.
